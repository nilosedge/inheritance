package net.nilosplace.inheritance.view;

public class View {

	public static class FieldsOnly { }
	
	public static class CreateView extends FieldsOnly { }
	public static class ReadView extends FieldsOnly { }
	public static class UpdateView extends FieldsOnly { }
	public static class DeleteView extends FieldsOnly { }
}
