package net.nilosplace.inheritance;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.eclipse.microprofile.openapi.annotations.Components;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

@ApplicationPath("/api")
@OpenAPIDefinition(info = @Info(description = "This is to Test Interface inheritance", title = "This is to Test Interface inheritance", version = "1.0 Alpha"), security = {
	@SecurityRequirement(name = "api_token") }, components = @Components(securitySchemes = {
		@SecurityScheme(securitySchemeName = "api_token", type = SecuritySchemeType.HTTP, description = "API Token", scheme = "bearer") }))
public class RestApplication extends Application {

}