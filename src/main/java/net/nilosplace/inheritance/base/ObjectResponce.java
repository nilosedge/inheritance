package net.nilosplace.inheritance.base;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.Data;

@Data
@Schema(name = "ObjectResponce", description = "POJO that represents the ObjectResponce")
public class ObjectResponce<E extends BaseEntity> {
	private E entity;
}
