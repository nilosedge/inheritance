package net.nilosplace.inheritance.base;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.fasterxml.jackson.annotation.JsonView;

import net.nilosplace.inheritance.view.View;

public interface BaseSearchableInterface<E extends BaseSearchableEntity> {

    @POST
    @Path("/search")
    @JsonView({ View.FieldsOnly.class })
    public SearchResponce<E> search(SearchRequest request);
    
}
