package net.nilosplace.inheritance.base;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.annotation.JsonView;

import net.nilosplace.inheritance.view.View;

public interface BaseCrudStringInterface<E extends BaseEntity> {

	@POST
	@Path("/")
	@JsonView({ View.CreateView.class })
	public ObjectResponce<E> create(E entity);

	@GET
	@Path("/{string}")
	@JsonView({ View.ReadView.class })
	public ObjectResponce<E> read(@PathParam("id") String id);

	@PUT
	@Path("/")
	@JsonView({ View.UpdateView.class })
	public ObjectResponce<E> update(E entity);

	@DELETE
	@Path("/{string}")
	@JsonView({ View.DeleteView.class })
	public ObjectResponce<E> delete(@PathParam("id") String id);
	
}
