package net.nilosplace.inheritance.base;

public class BaseCrudController<E extends BaseEntity> implements BaseCrudIdInterface<E>, BaseCrudStringInterface<E> {

	@Override
	public ObjectResponce<E> create(E entity) {
		return null;
	}

	@Override
	public ObjectResponce<E> read(Long id) {
		return null;
	}
	
	@Override
	public ObjectResponce<E> read(String id) {
		return null;
	}

	@Override
	public ObjectResponce<E> update(E entity) {
		return null;
	}

	@Override
	public ObjectResponce<E> delete(Long id) {
		return null;
	}
	
	@Override
	public ObjectResponce<E> delete(String id) {
		return null;
	}

}
