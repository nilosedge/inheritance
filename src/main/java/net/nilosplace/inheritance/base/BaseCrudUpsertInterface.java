package net.nilosplace.inheritance.base;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

import com.fasterxml.jackson.annotation.JsonView;

import net.nilosplace.inheritance.view.View;

public interface BaseCrudUpsertInterface<E extends BaseEntity> {

	@POST
	@Path("/upsert")
	@JsonView({ View.CreateView.class })
	public ObjectResponce<E> upsert(E entity);
	
}
