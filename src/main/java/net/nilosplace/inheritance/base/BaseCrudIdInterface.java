package net.nilosplace.inheritance.base;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import com.fasterxml.jackson.annotation.JsonView;

import net.nilosplace.inheritance.view.View;

public interface BaseCrudIdInterface<E extends BaseEntity> {

	@POST
	@Path("/")
	@JsonView({ View.CreateView.class })
	public ObjectResponce<E> create(E entity);

	@GET
	@Path("/{id}")
	@JsonView({ View.ReadView.class })
	public ObjectResponce<E> read(@PathParam("id") Long id);

	@PUT
	@Path("/")
	@JsonView({ View.UpdateView.class })
	public ObjectResponce<E> update(E entity);

	@DELETE
	@Path("/{id}")
	@JsonView({ View.DeleteView.class })
	public ObjectResponce<E> delete(@PathParam("id") Long id);
	
}
