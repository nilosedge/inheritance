package net.nilosplace.inheritance.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data @EqualsAndHashCode(callSuper = true)
public class BaseSearchableEntity extends BaseEntity {
	private String bodyType;
}
