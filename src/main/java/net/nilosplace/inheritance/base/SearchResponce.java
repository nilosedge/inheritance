package net.nilosplace.inheritance.base;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.Data;

@Data
@Schema(name = "SearchResponce", description = "POJO that represents the SearchResponce")
public class SearchResponce<E extends BaseEntity> {
	private List<E> results;
}
