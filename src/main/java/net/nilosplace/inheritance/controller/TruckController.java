package net.nilosplace.inheritance.controller;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import net.nilosplace.inheritance.base.BaseCrudUpsertController;
import net.nilosplace.inheritance.entity.TruckEntity;
import net.nilosplace.inheritance.interfaces.TruckInterface;

@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TruckController extends BaseCrudUpsertController<TruckEntity> implements TruckInterface {

}
