package net.nilosplace.inheritance.controller;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import net.nilosplace.inheritance.base.BaseSearchController;
import net.nilosplace.inheritance.entity.CarEntity;
import net.nilosplace.inheritance.interfaces.CarInterface;

@RequestScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CarController extends BaseSearchController<CarEntity> implements CarInterface {

}