package net.nilosplace.inheritance.interfaces;

import javax.ws.rs.Path;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import net.nilosplace.inheritance.base.BaseCrudIdInterface;
import net.nilosplace.inheritance.base.BaseCrudUpsertInterface;
import net.nilosplace.inheritance.entity.TruckEntity;

@Path("/truck")
@Tag(name = "Truck Endpoints")
public interface TruckInterface extends BaseCrudIdInterface<TruckEntity>, BaseCrudUpsertInterface<TruckEntity> {

}