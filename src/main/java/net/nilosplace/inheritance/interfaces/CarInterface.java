package net.nilosplace.inheritance.interfaces;

import javax.ws.rs.Path;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import net.nilosplace.inheritance.base.BaseCrudIdInterface;
import net.nilosplace.inheritance.base.BaseCrudStringInterface;
import net.nilosplace.inheritance.base.BaseCrudUpsertInterface;
import net.nilosplace.inheritance.base.BaseSearchableInterface;
import net.nilosplace.inheritance.entity.CarEntity;

@Path("/car")
@Tag(name = "Car Endpoints")
public interface CarInterface extends BaseSearchableInterface<CarEntity>,  BaseCrudUpsertInterface<CarEntity>, BaseCrudIdInterface<CarEntity>, BaseCrudStringInterface<CarEntity>  {


}