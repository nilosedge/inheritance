package net.nilosplace.inheritance.entity;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.nilosplace.inheritance.base.BaseSearchableEntity;

@Data @EqualsAndHashCode(callSuper = true)
@Schema(name = "CarEntity", description = "POJO that represents the CarEntity")
public class CarEntity extends BaseSearchableEntity {
	public String hood;
}
