package net.nilosplace.inheritance.entity;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.nilosplace.inheritance.base.BaseSearchableEntity;

@Data @EqualsAndHashCode(callSuper = true)
@Schema(name = "TruckEntity", description = "POJO that represents the TruckEntity")
public class TruckEntity extends BaseSearchableEntity {
	private String bedType;
}
